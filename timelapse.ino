//////////////////////////////////////////////////////////////////
//©2011 bildr
//Released under the MIT License - Please reuse change and share
//Using the easy stepper with your arduino
//use rotate and/or rotateDeg to controll stepper motor
//speed is any number from .01 -> 1 with 1 being fastest - 
//Slower Speed == Stronger movement
/////////////////////////////////////////////////////////////////


#define DIR_PIN 2
#define STEP_PIN 3
#define INTERVALLO_PIN A0 
#define SAMPLING_DELAY 200
#define SHOTS_NUMBER 100

void setup() { 
  Serial.begin(9600);
  pinMode(DIR_PIN, OUTPUT); 
  pinMode(STEP_PIN, OUTPUT); 
  pinMode(INTERVALLO_PIN, INPUT);
  
  analogReference(INTERNAL);
} 

void loop(){ 
  int input = 1023;
  
  while(input > 100){
    delay(SAMPLING_DELAY);
    input = analogRead(INTERVALLO_PIN);
  }
    
  rotate(800, 0.5);
  delay(1000);

  double count = 0;
  int interval = 0;
  input=1023;
    
  while(input > 100){
      input = analogRead(INTERVALLO_PIN);
      count++;
      delay(SAMPLING_DELAY);
  }
    
  rotate(800, 0.5);
  interval = (int) ((count-1)*SAMPLING_DELAY/1000 + 1);
  Serial.print(interval);
  Serial.println("s");
  
  for(int i=0; i<SHOTS_NUMBER; i++){
    delay(interval*1000 - 224);
    rotate(800, 0.5);
  }

}



void rotate(int steps, float speed){ 
  //rotate a specific number of microsteps (8 microsteps per step) - (negitive for reverse movement)
  //speed is any number from .01 -> 1 with 1 being fastest - Slower is stronger
  int dir = (steps > 0)? HIGH:LOW;
  steps = abs(steps);

  digitalWrite(DIR_PIN,dir); 

  float usDelay = (1/speed) * 70;

  for(int i=0; i < steps; i++){ 
    digitalWrite(STEP_PIN, HIGH); 
    delayMicroseconds(usDelay); 

    digitalWrite(STEP_PIN, LOW); 
    delayMicroseconds(usDelay); 
  } 

}
